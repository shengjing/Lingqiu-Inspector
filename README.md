# LingQiu Inspector | Unity编辑器插件

[![star](https://gitcode.com/shengjing/Lingqiu-Inspector/star/badge.svg)](https://gitcode.com/shengjing/Lingqiu-Inspector)

![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)
![Unity Version](https://img.shields.io/badge/Unity-2022%2B-57b9d3.svg)
![Status](https://img.shields.io/badge/Status-Active-brightgreen.svg)

✨ LingQiu Inspector是一个Unity编辑器插件，提供多个特性，构建更好用的编辑器！🚧 本项目正在积极开发中，欢迎 star ⭐️ 关注更新！

*等待一个看板娘~~~*

## 🎯 功能亮点

| 特性 | 说明 |
|-----|------|
| **BoolDropdown** | 将布尔值转换为直观的下拉选择 |
| **条件显示** | 根据字段状态动态显示属性 |
| **标签** | 自定义字段标签与提示信息 |
| **只读状态** | 可视化展示不可编辑字段 |
| **消息提示** | 在Inspector中添加醒目标注 |

## 🚀 快速入门

### 环境要求

- Unity 2022.3 LTS 或更高版本
- 推荐使用 Rider/VS2022 等现代IDE

### 安装指南

#### 方法一：通过 Git URL 安装

1. 打开 Unity Package Manager (`Window > Package Manager`)
2. 点击 ➕ 按钮选择 `Add package from git URL`
3. 输入仓库地址：`https://gitcode.com/shengjing/Lingqiu-Inspector.git`

#### 方法二：本地安装

1. [下载最新发行版](https://gitcode.com/shengjing/Lingqiu-Inspector/releases)
2. 解压到项目 `Packages` 目录
3. 重启 Unity 编辑器

### 使用指南

#### BoolDropdown

- 说明：

    用于标记布尔字段，并根据字段的值显示自定义下拉列表

- 示例：
    ```csharp
    [BoolDropdown("是", "否")]
    public bool ShowCharacter;
    ```

#### ShowIf

- 说明：

    根据布尔字段的值显示或隐藏该字段

- 示例：
    ```csharp
    [BoolDropdown("显示", "隐藏")] 
    public bool DisplaySettings = false;

    [ShowIf("DisplaySettings")]
    public Vector3 SpawnPosition;

    ```

#### ShowIfEnum

- 说明：

    根据枚举字段的值显示或隐藏该字段

- 示例：
    ```csharp
    public enum TestEnum
    {
        None,
        Element1,
        Element2,
        Element3
    }

    public TestEnum testEnum = TestEnum.None;

    // 当testEnum为Element1时，testString2显示
    [ShowIfEnum("testEnum", TestEnum.Element1)]
    public string testString2 = "testString2";
    ```

#### InspectorLabelText

- 说明：

    用于标记字段，并显示自定义的字段名称和提示。该属性可以添加到任何字段上

- 示例：
    ```csharp
    [InspectorLabelText("显示自定义字段", "显示自定义提示")]
    public string testString3 = "testString3";
    ```

#### ReadOnlyLabel

- 说明：

    ReadOnlyLabel属性工具，用于标记只读字段。该属性可以添加到任何字段上，并禁止在编辑器检查器面板中编辑该字段的值

- 示例：
    ```csharp
    [ReadOnlyLabel("这是一个只读字段")]
    public string readOnlyString = "readonlyString";
    ```

#### MessageLabel

- 说明：

    MessageLabel属性工具，用于标记字段，并显示一个提示信息。该属性可以添加到任何字段上，并在编辑器检查器面板显示

- 示例：

    ```csharp
    [MessageLabel("这是一个提示信息")]
    public string toolTipString = "这是一个带有提示的字段";
    ```


## 📜 开源协议

本项目采用 **Apache License 2.0** 开源协议，允许：
- 商业使用
- 修改发行
- 专利授权
- 个人使用

详细条款请查看 [LICENSE](./LICENSE) 文件。
#define LINGQIU
using System;
using UnityEngine;

namespace LingqiuInspector.Tools
{
    /// <summary>
    /// BoolDropdown属性工具，用于标记字段，并根据字段的值显示下拉列表
    /// 示例：[BoolDropdown("是", "否")]
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class BoolDropdownAttribute : PropertyAttribute 
    {
        public string trueText;
        public string falseText;

        /// <summary>
        /// 设置true和false的显示文本
        /// </summary>
        /// <param name="truetext">为true时显示的文本</param>
        /// <param name="falsetext">为false时显示的文本</param>
        public BoolDropdownAttribute(string truetext, string falsetext)
        {
            this.trueText = truetext;
            this.falseText = falsetext;
        }
    }

    /// <summary>
    /// ShowIf属性工具
    /// 示例：[ShowIf("ShowA")]
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class ShowIfAttribute : PropertyAttribute
    {
        /// <summary>
        /// 条件字段名称
        /// </summary>
        public string conditionFieldName;
        /// <summary>
        /// 条件字段值是否为真
        /// </summary>
        public bool showIfTrue = true;

        /// <summary>
        /// 设置条件字段名称和条件字段值是否为真，当条件字段值为真时，显示该字段
        /// </summary>
        /// <param name="conditionFieldName"></param>
        /// <param name="showIfTrue"></param>
        public ShowIfAttribute(string conditionFieldName, bool showIfTrue = true)
        {
            this.conditionFieldName = conditionFieldName;
            this.showIfTrue = showIfTrue;
        }
    }

    /// <summary>
    /// ShowIfEnum属性工具，用于用于标记字段，并根据枚举字段的值显示或隐藏该字段
    /// 示例：[LingqiuInspector.Tools.ShowIfEnum(nameof(currentType), SaveType.Type1)]
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class ShowIfEnumAttribute : PropertyAttribute
    {
        /// <summary>
        /// 枚举字段名称
        /// </summary>
        public string enumFieldName;
        /// <summary>
        /// 枚举值
        /// </summary>
        public object enumValue;

        /// <summary>
        /// 设置枚举字段名称和枚举值，当该字段的值等于枚举值时，显示该字段
        /// </summary>
        /// <param name="enumFieldName">字段名称</param>
        /// <param name="enumValue">枚举值</param>
        public ShowIfEnumAttribute(string enumFieldName, object enumValue)
        {
            this.enumFieldName = enumFieldName;
            this.enumValue = enumValue;
        }
    }

    /// <summary>
    /// Label文本属性工具，用于标记字段，并显示一个自定义的Label文本。该属性可以添加到任何字段上，并在编辑器检查器面板显示
    /// 示例：[InspectorLabelText("显示自定义字段", "显示自定义提示")]
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class InspectorLabelTextAttribute : PropertyAttribute
    {
        /// <summary>
        /// 自定义Label文本
        /// </summary>
        public string label;
        /// <summary>
        /// 提示文本
        /// </summary>
        public string tips;

        /// <summary>
        /// 设置自定义Label文本和提示文本
        /// </summary>
        /// <param name="label"></param>
        /// <param name="tips"></param>
        public InspectorLabelTextAttribute(string label, string tips = "")
        {
            this.label = label;
            this.tips = tips;
        }
    }

    /// <summary>
    /// ReadOnlyLabel属性工具，用于标记只读字段。该属性可以添加到任何字段上，并阻止在编辑器检查器面板中编辑该字段的值
    /// 示例：[ReadOnlyLabel("只读字段", UnityEditor.MessageType.Info)]
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class ReadOnlyLabelAttribute : PropertyAttribute
    {
        /// <summary>
        /// 只读字段的提示文本
        /// </summary>
        public string reason;

        /// <summary>
        /// 设置只读字段的提示种类和文本
        /// </summary>
        /// <param name="reason">提示文本</param>
        public ReadOnlyLabelAttribute(string reason = "")
        {
            this.reason = reason;
        }
    }


    /// <summary>
    /// MessageLabel属性工具，用于标记字段，并显示一个提示信息。该属性可以添加到任何字段上，并在编辑器检查器面板显示
    /// 示例：[MessageLabel("这是一个提示信息")]
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class MessageLabelAttribute : PropertyAttribute
    {
        /// <summary>
        /// 提示信息文本
        /// </summary>
        public string message;

        /// <summary>
        /// 设置提示信息和提示种类
        /// </summary>
        /// <param name="message">提示信息</param>
        public MessageLabelAttribute(string message)
        {
            this.message = message;
        }
    }
}

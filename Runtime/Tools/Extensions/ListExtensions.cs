using System;
using System.Collections.Generic;
using System.Linq;

namespace LingqiuInspector.Tools.Extensions
{
    /// <summary>
    /// 列表的扩展类
    /// </summary>
    public static class ListExtensions
    {
        /// <summary>
        /// 从列表中随机抽取一个元素，适用于没有权重的列表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static T GetRandomElement<T>(this List<T> list)
        {
            return list[UnityEngine.Random.Range(0, list.Count)];
        }

        /// <summary>  
        /// 打乱列表的顺序，返回一个新的列表
        /// </summary>  
        /// <typeparam name="T">列表元素的类型</typeparam>
        /// <param name="list">要被打乱的列表</param>
        /// <returns>返回一个新的被打乱顺序的列表</returns>
        /// <exception cref="ArgumentException">当该列表为空或为null时抛出异常</exception>
        public static List<T> Shuffle<T>(this List<T> list)
        {
            if (list == null || list.Count == 0)
            {
                throw new ArgumentException("该列表为空或为null");
            }

            Random rnd = new Random();
            // 用Fisher-Yates洗牌算法来打乱列表的顺序
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            return list;
        }

        /// <summary>
        /// 从列表中按权重抽取一个元素，适用于WeightedItem的列表
        /// </summary>
        /// <typeparam name="T">列表元素的类型</typeparam>
        /// <param name="list">要抽取元素的列表</param>
        /// <returns>返回一个随机抽取的元素</returns>
        /// <exception cref="ArgumentException">当该列表为空或为null时，以及当该列表的权重总和为0时抛出异常</exception>
        public static T RandomDrawWithWeight<T>(this List<WeightedItem<T>> list)
        {
            if (list == null || list.Count == 0)
            {
                throw new ArgumentException("该列表为空或为null");
            }

            int totalWeight = list.Sum(item => item.Weight);
            if (totalWeight <= 0)
            {
                throw new ArgumentException("该列表的权重总和为0");
            }

            int randomValue = UnityEngine.Random.Range(0, totalWeight);
            int weightSum = 0;

            // 遍历列表，根据权重来决定抽取哪个元素
            foreach (var item in list)
            {
                weightSum += item.Weight;
                if (randomValue < weightSum)
                {
                    return item.Item;
                }
            }
            return list.Last().Item;
        }

    }
}
﻿namespace LingqiuInspector.Tools.Extensions
{

    /// <summary>
    /// 数组扩展方法，该方法能够从数组中随机返回一个元素
    /// </summary>
    public static class ArrayExtensions
    {
        /// <summary>  
        /// 从数组中随机返回一个元素
        /// </summary>  
        /// <typeparam name="T">数组元素的类型</typeparam>  
        /// <param name="array">要从中随机返回元素的数组</param>  
        /// <returns>返回随机选中的数组元素，如果数组为空则返回默认值</returns>  
        public static T GetRandomElement<T>(this T[] array)
        {
            if (array == null || array.Length == 0)
            {
                // 返回默认值
                return default(T);
            }
            // 使用随机数生成器来选择一个随机索引
            System.Random rng = new System.Random();
            int randomIndex = rng.Next(array.Length);
            return array[randomIndex];
        }
    }
}
namespace LingqiuInspector.Tools.Extensions
{
    /// <summary>
    /// 权重列表类，用于存储带有权重的数据项
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class WeightedItem<T>
    {
        /// <summary>
        /// 数据项
        /// </summary>
        public T Item { get; private set; }
        /// <summary>
        /// 权重值
        /// </summary>
        public int Weight { get; private set; }

        public WeightedItem(T item, int weight)
        {
            Item = item;
            Weight = weight;
        }
    }
}
using System.Collections.Generic;
using UnityEngine;

namespace LingqiuInspector.Tools.Extensions
{
    /// <summary>
    /// GameObject的扩展方法类
    /// </summary>
    public static class GameObjectExtensions
    {
        /// <summary>
        /// 移除GameObject的所有子物体
        /// </summary>
        /// <param name="gameObject"></param>
        public static void RemoveAllChildren(this GameObject gameObject)
        {
            foreach (Transform child in gameObject.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        /// <summary>
        /// 获取所有子物体
        /// </summary>
        /// <param name="gameObject"></param>
        /// <returns>子物体列表</returns>
        public static List<GameObject> GetAllChildren(this GameObject gameObject)
        {
            List<GameObject> children = new List<GameObject>();
            // 如果没有子物体，直接返回空列表
            if (gameObject.transform.childCount == 0) return children;
            // 遍历所有子物体，并添加到列表中
            foreach (Transform child in gameObject.transform)
            {
                children.Add(child.gameObject);
            }
            return children;
        }

        /// <summary>
        /// 获取子物体的数量，等同于transform.childCount
        /// </summary>
        /// <param name="gameObject"></param>
        /// <returns>子物体数量</returns>
        public static int GetChildCount(this GameObject gameObject)
        {
            return gameObject.transform.childCount;
        }

        /// <summary>
        /// 获取所有父物体
        /// </summary>
        /// <param name="gameObject"></param>
        /// <returns></returns>
        public static List<GameObject> GetAllParents(this GameObject gameObject)
        {
            List<GameObject> parents = new List<GameObject>();
            Transform parent = gameObject.transform.parent;
            while (parent != null)
            {
                parents.Add(parent.gameObject);
                parent = parent.parent;
            }
            return parents;
        }

        /// <summary>
        /// 获取父物体的数量，等同于GetAllParents().Count
        /// </summary>
        /// <param name="gameObject"></param>
        /// <returns></returns>
        public static int GetParentCount(this GameObject gameObject)
        {
            return gameObject.GetAllParents().Count;
        }

        /// <summary>
        /// 获取指定名称的子物体，如果找不到则返回null，如果找到多个则返回第一个
        /// </summary>
        /// <param name="name">物体名称</param>
        /// <returns>子物体</returns>
        public static GameObject GetChildByName(this UnityEngine.GameObject gameObject, string name)
        {
            return gameObject.transform.Find(name)?.gameObject;
        }

        /// <summary>
        /// 获取组件，如果组件不存在则自动添加组件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gameObject"></param>
        /// <returns></returns>
        public static T GetOrAddComponent<T>(this GameObject gameObject) where T : UnityEngine.Component
        {
            return gameObject.GetComponent<T>() ?? gameObject.AddComponent<T>();
        }

        /// <summary>
        /// 打乱GameObject的所有子物体顺序
        /// </summary>  
        /// <param name="gameObject">要被打乱子物体顺序的GameObject</param>  
        public static void ShuffleChildren(this GameObject gameObject)
        {
            Transform parentTransform = gameObject.transform;
            List<Transform> childTransforms = new List<Transform>(parentTransform.childCount);

            // 先把所有子物体添加到列表中  
            for (int i = 0; i < parentTransform.childCount; i++)
            {
                childTransforms.Add(parentTransform.GetChild(i));
            }

            // 使用Fisher-Yates洗牌算法打乱子物体顺序  
            System.Random rng = new System.Random();
            for (int i = childTransforms.Count - 1; i > 0; i--)
            {
                int n = rng.Next(i + 1);
                Transform temp = childTransforms[i];
                childTransforms[i] = childTransforms[n];
                childTransforms[n] = temp;
            }

            // 将打乱顺序的子物体重新设置回父物体  
            for (int i = 0; i < childTransforms.Count; i++)
            {
                childTransforms[i].SetSiblingIndex(i);
            }
        }
    }
}
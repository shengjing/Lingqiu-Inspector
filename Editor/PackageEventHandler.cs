using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using UnityEngine;
using System.Collections.Generic;

[InitializeOnLoad]
public class PackageEventHandler
{
    private static readonly string PACKAGE_NAME = "com.zhanlehall.lingqiu-inspector";
    private static readonly string DEFINE_SYMBOL = "LINGQIU";

    static PackageEventHandler()
    {
        Events.registeredPackages += OnRegisteredPackages;
    }

    private static void OnRegisteredPackages(PackageRegistrationEventArgs args)
    {
        foreach (var package in args.added)
        {
            if (package.name == PACKAGE_NAME)
            {
                AddDefineSymbol();
            }
        }
    }

    private static void AddDefineSymbol()
    {
        string currentDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone);
        if (!currentDefines.Contains(DEFINE_SYMBOL))
        {
            if (string.IsNullOrEmpty(currentDefines))
                currentDefines = DEFINE_SYMBOL;
            else
                currentDefines += $";{DEFINE_SYMBOL}";

            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, currentDefines);
        }
    }
}
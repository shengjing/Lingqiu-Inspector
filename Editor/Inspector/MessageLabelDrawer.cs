﻿using UnityEngine;
using UnityEditor;

namespace LingqiuInspector.Tools
{
    /// <summary>
    /// 在检查器面板中显示消息标签
    /// </summary>
    [CustomPropertyDrawer(typeof(MessageLabelAttribute))]
    public class MessageLabelDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.PropertyField(position, property, label, true);
            MessageLabelAttribute messageLabelAttribute = (MessageLabelAttribute)attribute;

            string message = messageLabelAttribute.message;
            if (message != null)
            {
                var labelRect = new Rect(position.x, position.y + 19f, position.width, EditorGUIUtility.singleLineHeight);
                EditorGUI.HelpBox(labelRect, message, MessageType.Info);

            }
            else
            {
                Debug.LogError("在字段" + property.name + "上找到MessageLabelAttribute，但未设置消息。");
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = EditorGUI.GetPropertyHeight(property, label, true);
            MessageLabelAttribute messageLabelAttribute = (MessageLabelAttribute)attribute;
            if (!string.IsNullOrEmpty(messageLabelAttribute?.message))
            {
                height += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            }
            return height;
        }
    }
}
using UnityEngine;
using UnityEditor;


namespace LingqiuInspector.Tools
{
    /// <summary>
    /// ShowIfEnum属性，在检查器中根据枚举值显示或隐藏其他字段
    /// </summary>
    [CustomPropertyDrawer(typeof(ShowIfEnumAttribute))]
    public class ShowIfEnumDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            ShowIfEnumAttribute showIf = (ShowIfEnumAttribute)attribute;

            // 获取Enum字段的SerializedProperty对象
            SerializedProperty enumProperty = property.serializedObject.FindProperty(showIf.enumFieldName);
            if (enumProperty == null)
            {
                Debug.LogError("Enum字段没有找到" + showIf.enumFieldName);
                return;
            }

            // 检查枚举值是否匹配
            var enumValue = enumProperty.enumValueIndex;
            if (Equals(enumValue, (int)showIf.enumValue))
            {
                // 显示原始字段
                EditorGUI.PropertyField(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            // 如果条件不满足，返回0高度以隐藏字段
            ShowIfEnumAttribute showIf = (ShowIfEnumAttribute)attribute;
            SerializedProperty enumProperty = property.serializedObject.FindProperty(showIf.enumFieldName);
            if (enumProperty != null)
            {
                var enumValue = enumProperty.enumValueIndex;
                // 如果条件不满足，返回0高度以隐藏字段
                if (!Equals(enumValue, (int)showIf.enumValue))
                {
                    return 0f;
                }
            }
            return EditorGUI.GetPropertyHeight(property, label);
        }
    }
}
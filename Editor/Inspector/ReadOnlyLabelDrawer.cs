using UnityEditor;
using UnityEngine;


namespace LingqiuInspector.Tools
{
    /// <summary>
    /// 用于将字段设置为只读的特性。用于将字段设置为只读
    /// </summary>
    [CustomPropertyDrawer(typeof(ReadOnlyLabelAttribute))]
    public class ReadOnlyLabelDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // 使用默认的字段绘制方法，设置为只读  
            EditorGUI.BeginDisabledGroup(true); 
            { 
                EditorGUI.PropertyField(position, property, label, true); 
            }
            EditorGUI.EndDisabledGroup();
            // 显示只读的原因 
            ReadOnlyLabelAttribute readOnlyAttribute = (ReadOnlyLabelAttribute)attribute;
            if (!string.IsNullOrEmpty(readOnlyAttribute?.reason))
            {
                var labelRect = new Rect(position.x, position.y + 19f, position.width, EditorGUIUtility.singleLineHeight);
                EditorGUI.HelpBox(labelRect, readOnlyAttribute.reason, MessageType.Info);
            }

        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = EditorGUI.GetPropertyHeight(property, label, true);
            ReadOnlyLabelAttribute readOnlyAttribute = (ReadOnlyLabelAttribute)attribute;
            if (!string.IsNullOrEmpty(readOnlyAttribute?.reason))
            {
                height += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing + 10f;
            }
            return height;
        }
    }
}
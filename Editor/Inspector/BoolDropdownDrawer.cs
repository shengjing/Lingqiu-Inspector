using UnityEditor;
using UnityEngine;


namespace LingqiuInspector.Tools
{
    /// <summary>
    /// BoolDropdown属性，用于将bool类型的属性转换为下拉菜单
    /// </summary>
    [CustomPropertyDrawer(typeof(BoolDropdownAttribute))]
    public class BoolDropdownDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // 获取特性实例
            BoolDropdownAttribute dropdownAttribute = (BoolDropdownAttribute)attribute;
            // 绘制标签
            EditorGUI.LabelField(new Rect(position.x, position.y, EditorGUIUtility.labelWidth, position.height), label); 
            Rect dropdownPosition = new Rect(position.x + EditorGUIUtility.labelWidth, position.y, position.width - EditorGUIUtility.labelWidth, position.height);
            // 当前选中项true为0，false为1  
            int selectedIndex = property.boolValue ? 0 : 1;
            // 绘制下拉菜单并获取新的选中项  
            selectedIndex = EditorGUI.Popup(dropdownPosition, selectedIndex, new string[] 
            {
                (dropdownAttribute.trueText != "" ? dropdownAttribute.trueText : "True"),
                (dropdownAttribute.falseText!= "" ? dropdownAttribute.falseText : "False")
            });
            // 应用选择
            property.boolValue = selectedIndex == 0;
        }
    }

}
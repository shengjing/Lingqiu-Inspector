using UnityEditor;
using UnityEngine;

namespace LingqiuInspector.Tools
{
    /// <summary>
    /// ShowIf重绘
    /// </summary>
    [CustomPropertyDrawer(typeof(ShowIfAttribute))]
    public class ShowIfDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            ShowIfAttribute showIf = (ShowIfAttribute)attribute;
            SerializedObject serializedObject = property.serializedObject;

            SerializedProperty conditionProperty = serializedObject.FindProperty(showIf.conditionFieldName);
            if (conditionProperty == null)
            {
                Debug.LogError("无法找到属性：" + showIf.conditionFieldName);
                return;
            }

            bool show = (bool)conditionProperty.boolValue == showIf.showIfTrue;

            if (show)
            {
                EditorGUI.PropertyField(position, property, label);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            ShowIfAttribute showIf = (ShowIfAttribute)attribute;
            SerializedObject serializedObject = property.serializedObject;

            SerializedProperty conditionProperty = serializedObject.FindProperty(showIf.conditionFieldName);
            if (conditionProperty == null)
            {
                Debug.LogError("无法找到属性：" + showIf.conditionFieldName);
                return EditorGUIUtility.singleLineHeight;
            }

            bool show = (bool)conditionProperty.boolValue == showIf.showIfTrue;
            return show ? EditorGUI.GetPropertyHeight(property, label) : 0f;
        }
    }
}
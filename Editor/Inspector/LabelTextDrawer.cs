using UnityEditor;
using UnityEngine;

namespace LingqiuInspector.Tools
{
    /// <summary>
    /// 自定义属性绘制器，用于绘制带有LabelTextAttribute的属性字段
    /// </summary>
    [CustomPropertyDrawer(typeof(InspectorLabelTextAttribute))]
    public class LabelTextDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            InspectorLabelTextAttribute labelTextAttr = (InspectorLabelTextAttribute)attribute;

            // 使用自定义的标签文本
            GUIContent newLabel = new GUIContent(labelTextAttr.label, labelTextAttr.tips);

            // 绘制属性字段
            EditorGUI.PropertyField(position, property, newLabel);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label);
        }
    }
}